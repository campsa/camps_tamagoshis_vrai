package org.utils;

import java.util.Random;

public class RandomNumberGenerator {
    private final Random random = new Random();

    public int generateRandomNumber(int bound) {
        return random.nextInt(bound);
    }

    public int generateRandomNumber(int lowerBound, int upperBound) {
        if (lowerBound > upperBound) {
            throw new IllegalArgumentException("La borne inférieure doit être inférieure ou égale à la borne supérieure");
        }

        return random.nextInt(upperBound - lowerBound + 1) + lowerBound;
    }

    public double generateRandomDouble() {
        return random.nextDouble();
    }

}

package org.example;

import org.tamogoshis.Tamagoshis;
import org.tamogoshis.TamagoshisFactory;
import org.utils.RandomNumberGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Party {
    private static final Scanner scanner = new Scanner(System.in);
    private static final String NB_TAMA_INF_0 = "Le nombre doit être supérieur à 0. Fin du programme.";
    private static final String START_PARTY = " Entrez le nombre de tamagoshis désiré !\n" +
            "\n" +
            "Saisissez un nombre > 0 :";
    private static final String ENTER_NAME_TAMAGOSHIS = "Entrez le nom du tamagoshi numéro ";
    private static final String GAME_OVER = "Game Over !";

    private static Party instance;
    private List<Tamagoshis> tamagoshisList = new ArrayList<>();
    private RandomNumberGenerator randomNumberGenerator = new RandomNumberGenerator();

    private Party() {
        // Constructeur privé pour empêcher l'instanciation directe
    }

    public static Party getInstance() {
        if (instance == null) {
            instance = new Party();
        }
        return instance;
    }

    public void onStart() {
        System.out.println(START_PARTY);
        int numberOfTamagotchis = scanner.nextInt();
        scanner.nextLine();

        if (numberOfTamagotchis <= 0) {
            System.out.println(NB_TAMA_INF_0);
        } else {
            initPlayer(numberOfTamagotchis);
            playGame();
        }
    }

    private void initPlayer(int numberOfTamagotchis) {
        TamagoshisFactory tamagoshisFactory = new TamagoshisFactory(randomNumberGenerator);
        for (int i = 1; i <= numberOfTamagotchis; i++) {
            System.out.print(ENTER_NAME_TAMAGOSHIS + i + " : ");
            String tamagotchiName = scanner.nextLine();

            // Créer une instance de Tamagotchi avec le nom fourni
            Tamagoshis tamagoshis = tamagoshisFactory.createTamagoshis(tamagotchiName);
            tamagoshisList.add(tamagoshis);
        }
    }

    private void playGame() {
        while (!isGameOver()) {
            displayTamagotchisStatus();
            performTurn();
        }
        System.out.println(GAME_OVER);
    }

    private boolean isGameOver() {
        return tamagoshisList.isEmpty();
    }

    private void displayTamagotchisStatus() {
        System.out.println("-------- État actuel des Tamagotchis --------");
        for (Tamagoshis tamagoshis : tamagoshisList) {
            System.out.println(tamagoshis);
        }
        System.out.println("-------------------------------------------");
    }

    private void performTurn() {
        for (Tamagoshis tamagoshis : tamagoshisList) {
            System.out.println("Actions pour " + tamagoshis.getName() + ":");
            System.out.println("1. Nourrir");
            System.out.println("2. Jouer");
            System.out.println("3. Ignorer");

            int choice = scanner.nextInt();
            scanner.nextLine(); // Clear the newline character

            switch (choice) {
                case 1:
                    tamagoshis.feed();
                    break;
                case 2:
                    tamagoshis.play();
                    break;
                case 3:
                    // Do nothing (ignore)
                    break;
                default:
                    System.out.println("Choix invalide. Choisissez à nouveau.");
                    break;
            }

        }

        // Supprimer les Tamagotchis morts
        tamagoshisList.removeIf(Tamagoshis::isDead);
    }
}
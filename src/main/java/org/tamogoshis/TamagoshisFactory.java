package org.tamogoshis;

import org.utils.RandomNumberGenerator;

import java.util.ArrayList;
import java.util.Random;

/**
 * class permettant de créer des tamagoshis
 */
public class TamagoshisFactory {

    private RandomNumberGenerator randomNumberGenerator;

    public TamagoshisFactory(RandomNumberGenerator randomNumberGenerator) {
        this.randomNumberGenerator = randomNumberGenerator;
    }

    /**
     *
     * @param name nom du Tamagoshis
     * @return un Tamagoshis
     */
    public Tamagoshis createTamagoshis(String name) {

        Tamagoshis.TypeTamagoshis type = createRandomTamagoshis();
        switch (type){
            case COMMUN -> {
                return new TamogoshisCommun(name,randomNumberGenerator);
            }
            case BIGEATER -> {
                return new TamagoshisBigEater(name,randomNumberGenerator);
            }
            case LEGENDARY -> {
                return new TamagoshisLegendary(name,randomNumberGenerator);
            }
        }
        return new TamogoshisCommun(name,randomNumberGenerator);
    }

    private Tamagoshis.TypeTamagoshis createRandomTamagoshis() {
        //String[] possibleTypes = {"TypeA", "TypeB", "TypeC"};
        ArrayList<Tamagoshis.TypeTamagoshis> possibleTypes = new ArrayList<>();
        possibleTypes.add(Tamagoshis.TypeTamagoshis.COMMUN);
        possibleTypes.add(Tamagoshis.TypeTamagoshis.BIGEATER);
        possibleTypes.add(Tamagoshis.TypeTamagoshis.LEGENDARY);

        // Choisissez le type en fonction des probabilités
        return chooseRandomType(possibleTypes);
    }

    private  Tamagoshis.TypeTamagoshis chooseRandomType(ArrayList<Tamagoshis.TypeTamagoshis> possibleTypes) {
        double randomValue = randomNumberGenerator.generateRandomDouble();
        double cumulativeProbability = 0.0;

        for (int i = 0; i < possibleTypes.size(); i++) {
            cumulativeProbability += possibleTypes.get(i).getProbability();
            if (randomValue <= cumulativeProbability) {
                return possibleTypes.get(i);
            }
        }

        // Par défaut, retournez le dernier type (au cas où les probabilités ne totalisent pas 1.0)
        return possibleTypes.get(0);
    }
}

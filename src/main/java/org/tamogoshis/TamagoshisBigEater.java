package org.tamogoshis;

import org.utils.RandomNumberGenerator;

public class TamagoshisBigEater extends Tamagoshis{

    public static String TYPE = "bigEater";
    public TamagoshisBigEater(String name, RandomNumberGenerator randomNumberGenerator) {
        super(name, randomNumberGenerator);
    }
}

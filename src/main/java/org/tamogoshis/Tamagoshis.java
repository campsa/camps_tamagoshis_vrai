package org.tamogoshis;

import org.utils.RandomNumberGenerator;

public abstract class Tamagoshis {

    private String name;
    private RandomNumberGenerator randomNumberGenerator;
    private int age;

    private int cmptEnergy;
    private int maxEnergy;
    private int alerteEnergy;
    private int cmptFun;
    private int maxFun;
    private int alerteFun;

    public Tamagoshis(String name, RandomNumberGenerator randomNumberGenerator) {
        this.name = name;
        this.randomNumberGenerator = randomNumberGenerator;
        this.age = 0;
        this.cmptEnergy = randomNumberGenerator.generateRandomNumber(3,5);
        this.maxEnergy = randomNumberGenerator.generateRandomNumber(5,9);
        this.alerteEnergy = randomNumberGenerator.generateRandomNumber(3,5);
        this.cmptFun = randomNumberGenerator.generateRandomNumber(3,5);
        this.maxFun = randomNumberGenerator.generateRandomNumber(5,9);
        this.alerteFun = randomNumberGenerator.generateRandomNumber(3,5);

    }

    public String getName() {
        return name;
    }

    public enum TypeTamagoshis{
        COMMUN(0.5),
        BIGEATER(0.4),
        LEGENDARY(0.1);

        private final double probability;

        TypeTamagoshis(double probability) {
            this.probability = probability;
        }

        public double getProbability() {
            return probability;
        }
    }
    // Méthode pour nourrir le Tamagotchi
    public void feed() {
        if (cmptEnergy < maxEnergy) {
            cmptEnergy++;
            System.out.println(name + " a été nourri et son énergie a augmenté.");
        } else {
            System.out.println(name + " n'a pas faim pour le moment.");
        }
    }

    // Méthode pour jouer avec le Tamagotchi
    public void play() {
        if (cmptFun < maxFun) {
            cmptFun++;
            System.out.println(name + " a joué et s'est amusé.");
        } else {
            System.out.println(name + " n'a pas envie de jouer pour le moment.");
        }
    }

    public boolean isDead() {
        return cmptEnergy <= 0 || cmptFun <= 0;
    }


    @Override
    public String toString() {
        return "Tamagoshis{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", cmptEnergy=" + cmptEnergy +
                ", maxEnergy=" + maxEnergy +
                ", alerteEnergy=" + alerteEnergy +
                ", cmptFun=" + cmptFun +
                ", maxFun=" + maxFun +
                ", alerteFun=" + alerteFun +
                "type = " + this.getClass() +
                '}' +'\n';
    }
}
